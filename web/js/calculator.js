$(document).ready(function () {

    // Бонус за месяц
    var bonus_monthly;

    // Бонус ПРОДАЖИ
    var bonus_exclusive;

    // Бонус ПРОДАЖИ
    var bonus_sale;

    // Базовая зарплата
    var base_salary;

    var Q = {}, P1 = {}, P2 = {}, P3 = {}, B1 = {}, B2 = {}, B4 = {}, S2 = {};

    // Класс объектов
    var obj_class_1 = {}, obj_class_2 = {}, obj_class_3 = {}, obj_class_4 = {};
    var obj_lvl_1 = {}, obj_lvl_2 = {}, obj_lvl_3 = {}, obj_lvl_4 = {};

    /**
     * Геттер (получить результат из input)
     * @param identifier
     * @returns {Number}
     */
    function get(identifier) {
        return parseFloat($('#'+identifier).val());
    }

    /**
     * Сеттер (записать результат в input)
     * @param identifier
     * @param val
     * @returns {*|jQuery}
     */
    function set(identifier, val) {
        return $('#'+identifier).val(val);
    }

    /**
     * Формула расчета: Бонус за месяц
     * @param _bonus_exclusive
     * @param _bonus_sale
     * @param _P1
     * @param _P2
     * @param _P3
     * @returns {number}
     */
    function calcBonus_monthly(_bonus_exclusive, _bonus_sale, _P1, _P2, _P3) {
        var val = (_bonus_exclusive + _bonus_sale) * _P1.total_coefficient * _P2.total_coefficient * _P3.total_coefficient;

        // округление числа в большую сторону кратного 100
        val = Math.ceil(val);
        var division_residue = val % 100;
        if (division_residue !== 0) val = val - division_residue + 100;

        return val / 100; // % -> рубли
    }

    /**
     * Формула расчета: Бонус ЭСКЛЮЗИВЫ
     * @param _Q
     * @param _base_salary
     * @param _B1
     * @param _B2
     * @param _S2
     * @returns {number}
     */
    function calcBonus_exclusive(_Q, _base_salary, _B1, _B2, _S2) {
        var val = parseFloat(_Q.total_coefficient * _base_salary * (_B1.total_coefficient + _B2.total_coefficient) * _S2.total_coefficient);
        return val / 100; // % -> рубли
    }

    /**
     * Формула расчета: Бонус ПРОДАЖИ
     * @param _obj_class_1
     * @param _obj_class_2
     * @param _obj_class_3
     * @param _obj_class_4
     * @returns {Number}
     */
    function calcBonus_sale(_obj_class_1, _obj_class_2, _obj_class_3, _obj_class_4) {
        return parseFloat(_obj_class_1.bonus_sale + _obj_class_2.bonus_sale + _obj_class_3.bonus_sale + _obj_class_4.bonus_sale);
    }

    /**
     * Формула расчета: Классы (Бонус продажи)
     * @param _base_salary
     * @param _obj_lvl
     * @param _obj_class
     * @param _Q
     * @returns {Number}
     */
    function calcClass_bonus_sale(_base_salary, _obj_lvl, _obj_class, _Q) {
        return parseFloat(_base_salary * _obj_lvl.S1 * _obj_class.sold_objects * _Q.total_coefficient);
    }

    /**
     * Формула расчета: итоговое значение коээфициента для S2
     * @param _S2
     * @returns {Number}
     */
    function calcS2_total_coefficient(_S2) {
        return parseFloat(_S2.value_fact);
    }

    /**
     * Формула расчета: итоговое значение коээфициента для B2
     * @param _B
     * @returns {Number}
     */
    function calcB_total_coefficient(_B) {
        return parseFloat(_B.value_fact / _B.value_plan * _B.weight_in_total_result);
    }

    /**
     * Формула расчета: итоговое значение коээфициента для P2
     * @param _B1
     * @param _B2
     * @param _B4
     * @returns {Number}
     */
    function calcP2_total_coefficient(_B1, _B2, _B4) {
        var value_fact = _B1.value_fact + _B2.value_fact + _B4.value_fact;
        var value_plan = _B1.value_plan + _B2.value_plan + _B4.value_plan;
        return parseFloat(value_fact/value_plan * 100); // число -> %
    }

    /**
     * Инициализация
     */
    function init() {
        // базовая зарплата
        base_salary = get('base_salary');

        // Q
        Q.total_coefficient = get('Q_total_coefficient');

        // P1
        P1.total_coefficient = get('P1_total_coefficient');

        // P3
        P3.total_coefficient = get('P3_total_coefficient');

        // B1
        B1.weight_in_total_result = get('B1_weight_in_total_result');
        B1.value_plan = get('B1_value_plan');
        B1.value_fact = get('B1_value_fact');

        // B2
        B2.weight_in_total_result = get('B2_weight_in_total_result');
        B2.value_plan = get('B2_value_plan');
        B2.value_fact = get('B2_value_fact');

        // B4
        B4.value_plan = get('B4_value_plan');
        B4.value_fact = get('B4_value_fact');

        // S2
        S2.value_fact = get('S2_value_fact');

        // Классы (Количество проданных объектов)
        obj_class_1.sold_objects = get('Class1_sold_objects');
        obj_class_2.sold_objects = get('Class2_sold_objects');
        obj_class_3.sold_objects = get('Class3_sold_objects');
        obj_class_4.sold_objects = get('Class4_sold_objects');

        // Классы объектов
        obj_lvl_1.S1 = get('Lv1_S1');
        obj_lvl_1.S2 = get('Lv1_S2');
        obj_lvl_2.S1 = get('Lv2_S1');
        obj_lvl_2.S2 = get('Lv2_S2');
        obj_lvl_3.S1 = get('Lv3_S1');
        obj_lvl_3.S2 = get('Lv3_S2');
        obj_lvl_4.S1 = get('Lv4_S1');
        obj_lvl_4.S2 = get('Lv4_S2');
    }

    /**
     * Рассчитать все формулы
     */
    function calcAll() {
        // P2
        P2.total_coefficient = calcP2_total_coefficient(B1, B2, B4);

        // B1
        B1.total_coefficient = calcB_total_coefficient(B1);

        // B2
        B2.total_coefficient = calcB_total_coefficient(B2);

        // S2
        S2.total_coefficient = calcS2_total_coefficient(S2);

        // Классы (Бонус продажи)
        obj_class_1.bonus_sale = calcClass_bonus_sale(base_salary, obj_lvl_1, obj_class_1, Q);
        obj_class_2.bonus_sale = calcClass_bonus_sale(base_salary, obj_lvl_2, obj_class_2, Q);
        obj_class_3.bonus_sale = calcClass_bonus_sale(base_salary, obj_lvl_3, obj_class_3, Q);
        obj_class_4.bonus_sale = calcClass_bonus_sale(base_salary, obj_lvl_4, obj_class_4, Q);

        // Бонус ЭКСКЛЮЗИВЫ
        bonus_exclusive = calcBonus_exclusive(Q, base_salary, B1, B2, S2);

        // Бонус ПРОДАЖИ
        bonus_sale = calcBonus_sale(obj_class_1, obj_class_2, obj_class_3, obj_class_4);

        // Бонус за месяц
        bonus_monthly = calcBonus_monthly(bonus_exclusive, bonus_sale, P1, P2, P3);
    }

    /**
     * Записать результаты расчетов в input's
     */
    function setAll() {
        // P2
        set('P2_total_coefficient', P2.total_coefficient);

        // B1
        set('B1_total_coefficient', B1.total_coefficient);

        // B2
        set('B2_total_coefficient', B2.total_coefficient);

        // S2
        set('S2_total_coefficient', S2.total_coefficient);

        // Классы (Бонус продажи)
        set('Class1_bonus_sale', obj_class_1.bonus_sale);
        set('Class2_bonus_sale', obj_class_2.bonus_sale);
        set('Class3_bonus_sale', obj_class_3.bonus_sale);
        set('Class4_bonus_sale', obj_class_4.bonus_sale);

        // Бонус ЭКСКЛЮЗИВЫ
        set('bonus_exclusive', bonus_exclusive);

        // Бонус ПРОДАЖИ
        set('bonus_sale', bonus_sale);

        // Бонус за месяц
        set('bonus_monthly', bonus_monthly);
    }

    /**
     * Калькулятор: Получить + рассчитать + записать
     */
    function processCalculation() {
        init();
        calcAll();
        setAll();
    }

    /**
     * При любом изменение значения input запускать калькулятор
     */
    $('input').on('change', function () {
        processCalculation();
    });

    // При открытии страницы
    processCalculation();

    // активировать вплывающие подсказки
    $('[data-toggle="tooltip"]').tooltip()
});