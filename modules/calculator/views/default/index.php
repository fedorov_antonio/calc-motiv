<?php
use \yii\helpers\Html;
use \app\modules\calculator\assets\CalculatorAsset;

/* @var $this yii\web\View */

CalculatorAsset::register($this);
$this->title = 'Калькулятор мотивации';

?>

<style>
    thead th {
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-md-12 mt-5 mb-5">
        <h1><?= Html::encode($this->title)?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="table-responsive table-bordered">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th colspan="2">Мотивация</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Бонус за месяц</th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'bonus_monthly', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Бонус <span>эксклюзив</span></th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'bonus_exclusive', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Бонус <span>продажи</span></th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'bonus_sale', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Базовая з/п</th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 25000, ['class' => 'form-control', 'id' => 'base_salary'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12 mt-5">

        <div class="table-responsive table-bordered">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Коэффициент</th>
                    <th>Вес в общем результате</th>
                    <th><span>Значение план</span></th>
                    <th><span>Значение факт</span></th>
                    <th><span>Итоговое значение коэффициента</span></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">
                        Q
                        <span class="text-muted small float-right" data-toggle="tooltip" data-placement="top" title="см.таблицу классов"><i class="fas fa-calculator"></i></span>
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="Должность"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <?= Html::input('number', null, 0.6, ['class' => 'form-control', 'step' => '0.1', 'id' => 'Q_total_coefficient'])?>
                    </td>
                </tr>

                <tr>
                    <th class="bg-warning text-center" colspan="7">Коээфициенты выполения планов</th>
                </tr>
                <tr>
                    <th scope="row">
                        P1
                        <span class="text-muted small float-right" data-toggle="tooltip" data-html="true" data-placement="top" title="< 80% - 0.8 <br/>90%-110% - 1<br/>> 110% - 1.2"><i class="fas fa-calculator"></i></span>
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="Выполнение плана командой"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <?= Html::input('number', null, 1, ['class' => 'form-control', 'id' => 'P1_total_coefficient', 'step' => 0.2, 'min' => 0.8, 'max' => 1.2])?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        P2
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="Выполнение личных планов"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 100, ['class' => 'form-control', 'id' => 'P2_total_coefficient', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        P3
                        <span class="text-muted small float-right" data-toggle="tooltip" data-placement="top" title="0.7-1.2"><i class="fas fa-calculator"></i></span>
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="Субъективная оценка качества работы сотрудника руководителем (1,2 - пятерка с плюсом, 1,1 - пятерка, 1 - четверка, 0,9 - тройка, 0,8 -двойка, 0,7 - кол)"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <?= Html::input('number', null, 1, ['class' => 'form-control', 'id' => 'P3_total_coefficient', 'step' => 0.1, 'min' => 0.7, 'max' => 1.2])?>
                    </td>
                </tr>

                <tr>
                    <th class="bg-warning text-center" colspan="7">Бонус <span>эксклюзивы</span></th>
                </tr>
                <tr>
                    <th scope="row">
                        B1
                        <span class="text-muted small float-right" data-toggle="tooltip" data-placement="top" title="Факт / План x Вес"><i class="fas fa-calculator"></i></span>
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="План по количеству подписанных контрактов на продажу (эксклюзивный контракт - 1, неэксклюзив - 0,5)"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 60, ['class' => 'form-control', 'id' => 'B1_weight_in_total_result'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <?= Html::input('number', null, 4, ['class' => 'form-control', 'id' => 'B1_value_plan'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 4, ['class' => 'form-control', 'id' => 'B1_value_fact'])?>
                    </td>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'B1_total_coefficient', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        B2
                        <span class="text-muted small float-right" data-toggle="tooltip" data-placement="top" title="Факт (ср.арифм) / План x Вес"><i class="fas fa-calculator"></i></span>
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="Размер комиссионных - среднее арифметическое всех контрактов"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 40, ['class' => 'form-control', 'id' => 'B2_weight_in_total_result'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <?= Html::input('number', null, 4, ['class' => 'form-control', 'id' => 'B2_value_plan'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 4, ['class' => 'form-control', 'id' => 'B2_value_fact'])?>
                    </td>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'B2_total_coefficient', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        S2
                        <span class="text-muted small float-right" data-toggle="tooltip" data-placement="top" title="Значение из таблицы КЛАСС Объектов - Суммарная стоимость объектов / Кол-во объектов"><i class="fas fa-calculator"></i></span>
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="Дополнительный бонус за классность объекта"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <?= Html::input('number', null, 1, ['class' => 'form-control', 'id' => 'S2_value_fact'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'S2_total_coefficient', 'disabled' => 'disabled'])?>
                    </td>
                </tr>

                <tr>
                    <th class="bg-warning text-center" colspan="7">Бонус <span>продажи</span></th>
                </tr>
                <tr>
                    <th scope="row">
                        B4
                        <span class="text-muted small float-right mr-1" data-toggle="tooltip" data-placement="top" title="План по количеству проданных объектов"><i class="far fa-question-circle"></i></span>
                    </th>
                    <td>&nbsp;</td>

                    <td>
                        <?= Html::input('number', null, 1, ['class' => 'form-control', 'id' => 'B4_value_plan'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 1, ['class' => 'form-control', 'id' => 'B4_value_fact'])?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12 mt-5">

        <div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                <tr class="bg-primary text-white">
                    <th>&nbsp;</th>
                    <th>1 класса</th>
                    <th>2 класса</th>
                    <th>3 класса</th>
                    <th>4 класса</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Количество проданных объектов</th>
                    <td>
                        <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class1_sold_objects'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class2_sold_objects'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class3_sold_objects'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class4_sold_objects'])?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Бонус <span>продажи</span></th>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class1_bonus_sale', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>

                    </td>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class2_bonus_sale', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>

                    </td>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class3_bonus_sale', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>

                    </td>
                    <td>
                        <div class="input-group">
                            <?= Html::input('number', null, 0, ['class' => 'form-control', 'id' => 'Class4_bonus_sale', 'disabled' => 'disabled'])?>
                            <div class="input-group-append">
                                <span class="input-group-text">руб.</span>
                            </div>
                        </div>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12 mt-5">

        <div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                <tr class="bg-success text-white">
                    <th colspan="4">Класс объектов</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <th>Цена объекта</th>
                    <th>S1</th>
                    <th>S2</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>1 уровень</th>
                    <td>< 4 млн.руб.</td>
                    <td>
                        <?= Html::input('number', null, 1.3, ['class' => 'form-control', 'id' => 'Lv1_S1', 'disabled' => 'disabled'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 1, ['class' => 'form-control', 'id' => 'Lv1_S2', 'disabled' => 'disabled'])?>
                    </td>
                </tr>
                <tr>
                    <th>2 уровень</th>
                    <td>4 - 11 млн.руб.</td>
                    <td>
                        <?= Html::input('number', null, 2, ['class' => 'form-control', 'id' => 'Lv2_S1', 'disabled' => 'disabled'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 1.3, ['class' => 'form-control', 'id' => 'Lv2_S2', 'disabled' => 'disabled'])?>
                    </td>
                </tr>
                <tr>
                    <th>3 уровень</th>
                    <td>11 - 33 млн.руб.</td>
                    <td>
                        <?= Html::input('number', null, 5, ['class' => 'form-control', 'id' => 'Lv3_S1', 'disabled' => 'disabled'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 1.5, ['class' => 'form-control', 'id' => 'Lv3_S2', 'disabled' => 'disabled'])?>
                    </td>
                </tr>
                <tr>
                    <th>4 уровень</th>
                    <td>< 30 млн.руб.</td>
                    <td>
                        <?= Html::input('number', null, 20, ['class' => 'form-control', 'id' => 'Lv4_S1', 'disabled' => 'disabled'])?>
                    </td>
                    <td>
                        <?= Html::input('number', null, 2, ['class' => 'form-control', 'id' => 'Lv4_S2', 'disabled' => 'disabled'])?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>