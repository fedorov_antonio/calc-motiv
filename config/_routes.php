<?php

return [
    // Общие
    [
        'pattern' => '<controller>/<action>',
        'route'   => '<controller>/<action>',
    ],
    [
        'pattern' => '<module>/<controller>/<action>',
        'route'   => '<module>/<controller>/<action>',
    ],
];