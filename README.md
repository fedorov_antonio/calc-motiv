Описание проекта
================================

Калькулятор мотивации

Подраздел: Git проекта 
------------------------- 

https://fedorov_antonio@bitbucket.org/fedorov_antonio/calc-motiv.git

Подраздел: Production 
-------------------------

http://fedorovau.beget.tech

Подраздел: Роли, разрешения и права.
-------------------------

Модуль Calculator имеет ограничение на вход. Авторизация происходит на уровне AccessControl. 

Используется штатная app/models/User. Тут же и хранится 1 пользователь для входа:

Username: drozdovich

Password: @drozdovich

Раздел: Подготовка и настройка VDS
================================

Настройки: app/config/web.php
--------------------------------

```php
'components' => [
    ...
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => require __DIR__ . '/_routes.php',
    ],
    'assetManager' => [
        'bundles' => [
            'yii\bootstrap\BootstrapAsset' => [
                'sourcePath' => '@vendor/twbs/bootstrap/dist'
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'sourcePath' => '@vendor/twbs/bootstrap/dist'
            ],
            'yii\bootstrap\BootstrapThemeAsset' => [
                'sourcePath' => '@vendor/twbs/bootstrap/dist'
            ],
        ]
    ],
    ...
],
'modules' => [
    'calculator' => [
        'class' => 'app\modules\calculator\Module',
    ],
],
```


Обязательные пакеты Composer:
--------------------------------

composer require "twbs/bootstrap": "4.3.1"

Требования к серверу и ПО:
--------------------------------

1. Операционная система: CentOS 7
2. Версия языка PHP: 7.2
3. СУБД: MySQL 3.4
4. Веб-сервер: Apache
5. Пакет: git
6. Пакет: composer
7. Пакет: zip

Раздел: Установка и обновление проекта
================================

Подраздел: Установка проекта с нуля и первичные настройки Production 
--------------------------------

1. Зайти на сервер:
```text
ssh USERNAME@HOST
```

3.Перемещение в папку проекта:

```text
cd FOLDER_PROJECT
```

4.Скачивание проекта с git

```text
git init
git remote add origin GIT_PATH
git pull origin master
```

5.Обновление зависимостей 

```text
composer clear-cache
composer global require "fxp/composer-asset-plugin:*"
composer update
```

8. Создание символьных ссылок при необходимости 

```text
ln -s web public_html
```

Раздел: Разработчик 
================================

Антон, 8 (999) 700-70-03, fedorovau2012@gmail.com